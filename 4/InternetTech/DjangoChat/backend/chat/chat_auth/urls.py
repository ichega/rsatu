# urls.py
from django.urls import path
from .views import IndexView, ForgotView, SignUpView

urlpatterns = [
    path('', IndexView.as_view()),
    path('sign_up/', SignUpView.as_view()),
    path('forgot/', ForgotView.as_view()),
]