from preprocess import prepare
import json
from gam import GAM

import vk_api
from vk_api import VkUpload
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.utils import get_random_id

dataset = json.loads(open('assets/dataset.json', 'r').read())
words = prepare()

gam = GAM(len(words), words)
gam.train(dataset)
print(gam.predicate("поставь будильник на сегодня"))


def main():
    vk_session = vk_api.VkApi(
        token='dc36bf587be6e822b84a982606c6086c0a27bbe552b4b5b4b90faadbd2a932e2bfb5d88593cb9903275cb')

    vk = vk_session.get_api()

    upload = VkUpload(vk_session)  # Для загрузки изображений
    longpoll = VkLongPoll(vk_session)

    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            event.text = str(event.text)
            print('id{}: "{}"'.format(event.user_id, event.text), end=' ')
            answer = gam.predicate(event.text)[1]
            vk.messages.send(
                user_id=event.user_id,
                random_id=get_random_id(),
                message=answer.encode('utf-8')
            )
            print('ok')


if __name__ == '__main__':
    main()
