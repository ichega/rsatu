import json


def prepare():
    words = []
    for l in open('assets/russian_words_all.txt', 'r').readlines():
        words.append(l.replace("\n", ""))
    dataset = json.loads(open('assets/dataset.json', 'r').read())
    real_words = []

    for expr in dataset["commands"]:
        lw = expr.split()
        for word in lw:

            if word in words:

                if word not in real_words:
                    real_words.append(word)
    for expr in dataset["data"]:
        lw = expr[0].split()
        for word in lw:
            if word in words:
                if word not in real_words:
                    real_words.append(word)

    return real_words
