import numpy as np


class GAM:
    def __init__(self, size=1, words=[]):
        self.words = words
        self.size = size
        self.inputs = np.array([[0 for i in range(size)]], dtype=float)
        self.inputs1 = np.array([[0 for i in range(size)]], dtype=float)
        self.links = np.array([[0 for i in range(size)] for i in range(size)], dtype=float)

    def predicate(self, expr=""):
        # results = []

        expr = expr.split()
        self.inputs.fill(-1)

        for word in expr:
            if word in self.words:
                self.inputs[0][self.words.index(word)] = 1
        for i in range(5):
            self.inputs1 = np.dot(self.inputs, self.links)
            self.inputs = self.inputs1
        for i in range(self.size):
            if self.inputs1[0][i] < 0:
                self.inputs1[0][i] = -1
            elif self.inputs1[0][i] >= 0:
                self.inputs1[0][i] = 1

        answer = []
        for i in range(self.size):
            if self.inputs1[0][i] > 0:
                answer.append(self.words[i])
        answer = " ".join(answer)

        return [self.inputs1[0], answer]

    def train(self, dataset):

        for sample in dataset["data"]:
            self.inputs.fill(-1)
            expr = sample[0].split()
            for word in expr:
                if word in self.words:
                    self.inputs[0][self.words.index(word)] = 1
            cmd = dataset["commands"][sample[1]]
            self.inputs1.fill(-1)
            expr = cmd.split()
            for word in expr:
                if word in self.words:
                    self.inputs1[0][self.words.index(word)] = 1
            self.links += np.matmul(self.inputs.T, self.inputs1)



