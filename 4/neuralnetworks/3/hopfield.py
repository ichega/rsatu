import numpy as np
import math


class Hopfield:
    def __init__(self, size=1):
        self.size = size
        self.neurons = np.array([0 for x in range(size)], dtype=np.float)
        self.links = np.array([[0 for _1 in range(len(self.neurons))] for _2 in range(len(self.neurons))],
                              dtype=np.float)

    def predicate(self, string_input="", steps=100):
        inputs = np.array([-1.0 if c == "0" else 1.0 for c in string_input])
        self.neurons = inputs
        x = 0
        while x < steps:
            x += 1
            self.neurons = self.links.dot(self.neurons)

            for i in range(len(self.neurons)):
                self.neurons[i] = 1.0 if self.neurons[i] >= 0 else -1.0

        return "".join(["0" if i < 0 else "1" for i in self.neurons])

    def train(self, dataset=[]):
        if dataset["size"] < 1:
            return
        for d in range(dataset["size"]):
            img = [-1.0 if c == "0" else 1.0 for c in dataset["data"][d]]
            img = np.array(img)
            self.links = np.add(self.links, np.outer(img, img.transpose()))

        self.links = np.multiply(self.links, 1.0 / self.size)
        for i in range(self.size):
            self.links[i][i] = 0.0
