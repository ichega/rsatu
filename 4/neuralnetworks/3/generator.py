from PIL import Image

for i in range(10):

    im = Image.open(f'./dataset1/{i}.png')
    pix = im.load()
    for y in range(10):
        for x in range(10):
            p = pix[y,x]
            c = 0 if p[0] == 0 else 1
            print(c, end="")
    print()

