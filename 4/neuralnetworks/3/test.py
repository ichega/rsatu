from hopfield import Hopfield
import numpy as np
import json
from PIL import Image


def pp(s):
    i = 0
    for c in s:
        print(c, end="")
        i += 1
        if i == 10:
            print()
            i = 0
    print()


dataset = json.loads(open('dataset1.json').read())

h = Hopfield(dataset["length"])

print(h.links)
h.train(dataset)
print(h.links)

print(h.predicate("0000000000"))
print(h.predicate("1111111111"))
print(h.predicate("0011001100"))
print(h.predicate("0000011111"))
print(h.predicate("1111100000"))

print(h.predicate("1111100001"))
print(h.predicate("0011001101"))
print(h.predicate("1110001010", 2))
#
