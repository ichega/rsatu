from hopfield import Hopfield
import numpy as np
import json
from PIL import Image


def pp(s):
    i = 0
    for c in s:
        print(c,end="")
        i+=1
        if i == 10:
            print()
            i = 0
    print()

dataset = json.loads(open('dataset.json').read())

h = Hopfield(dataset["length"])

h.train(dataset)

pp(h.predicate("0001110000000111000000011100000000000000000000000000000000000000000000000000000000000000000000000000", 3))
print(h.links)


