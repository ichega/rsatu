import numpy as np
import math
import random
import json
import pickle


class Perceptron(object):

    def __init__(self, input_size, output_size, threshold=2000, learning_rate=0.01):
        self.threshold = threshold
        self.learning_rate = learning_rate
        self.outputs = np.zeros(output_size)
        self.weights = np.zeros((input_size, output_size))

    def init_weights(self):
        for weight in self.weights:
            for k, v in enumerate(weight):
                weight[k] = 0.0
                weight[k] = random.random() * 2.0 - 1.0

    def save(self, filename):
        pickle.dump(self.weights, filename)

    def load(self, filename):
        pickle.dump(self.weights, filename)

    def predict(self, inputs):
        outputs = np.dot(np.array(inputs), self.weights)
        for k, v in enumerate(outputs):
            outputs[k] = self.activate(v)
        result = np.where(outputs == np.amax(outputs))

        return outputs, result[0][0]

    def activate(self, x):
        # return math.tanh(x/10)
        return 1.0 / (1.0 + math.exp(-x))

    def train(self, dataset, logs=False):
        for epoch in range(self.threshold):
            errors = 0
            for data in dataset["dataset"]:
                prediction = self.predict(data["inputs"])[0]
                max_idx = np.where(prediction == np.amax(prediction))[0][0]
                if logs:
                    print('---Test---')
                    print(prediction)
                    print(max_idx)
                    print(data["outputs"])
                if data["outputs"][max_idx] != 1:
                    errors += 1
                    for ik, iv in enumerate(data["inputs"]):
                        for ok, ov in enumerate(data["outputs"]):
                                self.weights[ik][ok] += self.learning_rate * ov * iv

                    prediction = self.predict(data["inputs"])[0]
                    max_idx = np.where(prediction == np.amax(prediction))[0][0]
                    if logs:
                        print('---Check---')

                        print(prediction)
                        print(max_idx)
                        print(data["outputs"])
                        print(self.weights)


            if errors == 0:
                print(f'Сеть была обучена за {epoch} эпох!')
                return
        print(f'Сеть не была обучена')


dataset = open('dataset.json', 'r').read()
dataset = json.loads(dataset)

p = Perceptron(dataset["meta"]["inputs"], dataset["meta"]["outputs"])
# print(p.weights)
p.init_weights()
# print(p.weights)
print(p.predict([1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1]))
p.train(dataset)
print(p.predict([1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1]))
# print(p.predict([0, 1, 0, 0, 0, 0, 0, 0]))
# print(p.weights)
# print(p.outputs)
