//Imports Apps or Libs
import Vue from 'vue';
import Vuex from 'vuex';
import BootstrapVue from 'bootstrap-vue'
import VueRouter from 'vue-router'


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.use(Vuex);

Vue.use(VueRouter);
const store = new Vuex.Store({
    state: {
        editor: 0,
        layers: [],
        selected: 0,
        update_required: true,
        layer_id: 1,
        tool: 0,
    },

    mutations: {
        pick_layer(state, id) {
            state.selected = id;
            this.commit('require_update');
        },
        layer_up(state, id) {
            if (id === 0) return;
            let ref = state.layers[id - 1];
            state.layers[id - 1] = state.layers[id];
            state.layers[id] = ref;
            if (state.selected == id){
                state.selected -= 1;
            }
            else
            if (state.selected === id - 1){
                state.selected = id;
            }

            this.commit('require_update');
        },
        layer_down(state, id) {
            if (id === state.layers.length - 1) return;
            let ref = state.layers[id + 1];
            state.layers[id + 1] = state.layers[id];
            state.layers[id] = ref;
            if (state.selected == id){
                state.selected += 1;
            }
            else
            if (state.selected === id + 1){
                state.selected = id;
            }
            this.commit('require_update');
        },
        layer_hide(state, id){
            state.layers[id].visible = false;
        },
        layer_show(state, id){
            state.layers[id].visible = true;
        },
        add_layer(state, name) {
            let layer = {
                // name: name,
                name: `Layer ${state.layer_id}`,
                visible: true,
                canvas: document.createElement('canvas')
            };
            state.layer_id += 1;
            layer.canvas.width = state.editor.canvas.width;
            layer.canvas.height = state.editor.canvas.height;
            state.layers.push(layer);
            this.commit('require_update');
        },
        remove_layer(state, id){
            state.layers.splice(id, 1);
        },
        require_update(state) {
            state.update_required = true;
            console.log('RU');
            this.state.editor.$forceUpdate();
        },
        updated(state){
            state.update_required = false;
        },
        set_editor(state, editor){
            state.editor = editor;
        },
        pick_tool(state, tool){
            if ((tool < 0) || (tool > 3))
                return;
            state.tool = tool;
        }
    }
});
//Creating a Vue Instance
import App from './components/AppEditor.vue'

let AppStore = {
    extends: App,
    store,
};
let app = new Vue(AppStore).$mount('#editor');

//Test log
console.log('Its work!!!');