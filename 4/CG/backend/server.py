from sanic import Sanic
from sanic.response import json, html, redirect
from jinja2 import Environment, FileSystemLoader
import json, uuid
import os
import cv2
env = Environment(loader=FileSystemLoader('./templates'))
app = Sanic()
app.static('/static', './static')
app.static('/ge/static', './static')
app.static('/cvf/static', './static')
app.static('/transform/static', './static')
app.static('/frontend', './frontend')

@app.route("/")
async def index1(request):
    return redirect('/ge')

@app.route("/ge")
async def indexge(request):
    template = env.get_template('indexge.html')
    return html(template.render())

@app.route("/transform")
async def indextr(request):
    template = env.get_template('index_t.html')
    return html(template.render())

@app.route("/cvf")
async def indexcvf(request):
    template = env.get_template('index.html')
    return html(template.render())

@app.route("/cvf/proc", methods=['POST'])
async def indexproc(request):
    file = request.files.get('myFile')
    print(request.form)
    # print(file)
    id = uuid.uuid4()
    os.mkdir(f'static/images/{id}')

    filename = f'static/images/{id}/{file.name}'
    with open(filename, 'wb') as f:
        f.write(file.body)

    #denoising
    h = int(request.form.get('h'))
    hcolor = int(request.form.get('hcolor'))
    tws = int(request.form.get('tws'))
    sws = int(request.form.get('sws'))
    img = cv2.imread(filename)
    dst = cv2.fastNlMeansDenoisingColored(img, None, h, hcolor, tws, sws)
    denoise_filename = f'static/images/{id}/denoise_{file.name}'
    cv2.imwrite(denoise_filename, dst)

    #edge_detection
    threshold1 = int(request.form.get('threshold1'))
    threshold2 = int(request.form.get('threshold2'))
    edges = cv2.Canny(dst, threshold1, threshold2)

    edges_filename = f'static/images/{id}/edges_{file.name}'
    cv2.imwrite(edges_filename, edges)


    template = env.get_template('result.html')
    return html(template.render(default=filename, denoise=denoise_filename, edges=edges_filename))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)