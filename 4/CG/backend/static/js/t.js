// let file_url = prompt("Введите рабочий URL изображения:");
// let img = new Image;
// img.onload = function () {
//     console.log('Ok');
//     let canvas = document.getElementById("canvas");
//     console.log(this.attributes);
//     canvas.width = this.width;
//     canvas.height = this.height;
//     let context = canvas.getContext('2d');
//     context.drawImage(this, 0, 0);
//     console.log(canvas.toDataURL('image/jpeg'));
//     // let image_canvas = document.createElement('canvas');
//     // image_canvas.width = this.width;
//     // image_canvas.height = this.height;
//     // this.crossOrigin = "Anonymous";
//
//     // let image_context = image_canvas.getContext('2d');
//     // image_context.putImageData(context.getImageData(0, 0, this.width, this.height), 0, 0);
//
//
// }.bind(img);
// img.src = file_url;
//
scale = 0.5;

function to_scale(image, cnv, s) {
    cnv.width = Math.round(image.width * s);
    cnv.height = Math.round(image.height * s);
    var imageData = image.getContext('2d').getImageData(0, 0, image.width, image.height);
    var imageDataCNV = cnv.getContext('2d').getImageData(0, 0, cnv.width, cnv.height);
    var data = imageData.data;
    var datac = imageDataCNV.data;
    var x, y, red, green, blue, alpha;
    let ctx = cnv.getContext('2d');
    let ctx1 = image.getContext('2d');
    for (y = 0; y < cnv.height; y++) {
        for (x = 0; x < cnv.width; x++) {
            for (x = 0; x < cnv.width; x++) {
                // ctx.putImageData(ctx1.getImageData(x * (1.0/s), y * (1.0/s), 1,1), x, y);
                let id = ((cnv.width * y) + x) * 4;
                let x1 = Math.round(image.width * (x / cnv.width));
                let y1 = Math.round(image.height * (y / cnv.height));
                let id1 = ((image.width * y1) + x1) * 4;
                datac[id] = data[id1];
                datac[id+1] = data[id1+1];
                datac[id+2] = data[id1+2];
                datac[id+3] = data[id1+3];

            }
        }
        cnv.getContext('2d').putImageData(imageDataCNV, 0, 0);

    }
}

function to_rotate(image, cnv, angle) {
    cnv.width = image.width;
    cnv.height = image.height;
    var imageData = image.getContext('2d').getImageData(0, 0, image.width, image.height);
    var imageDataCNV = cnv.getContext('2d').getImageData(0, 0, cnv.width, cnv.height);
    var data = imageData.data;
    var datac = imageDataCNV.data;
    var x, y, red, green, blue, alpha;
    let ctx = cnv.getContext('2d');
    let ctx1 = image.getContext('2d');
    angle = (angle / 360.0 ) * 2 * Math.PI;
    for (y = 0; y < cnv.height; y++) {
        for (x = 0; x < cnv.width; x++) {
            for (x = 0; x < cnv.width; x++) {
                // ctx.putImageData(ctx1.getImageData(x * (1.0/s), y * (1.0/s), 1,1), x, y);
                let id = ((cnv.width * y) + x) * 4;
                let x1 = Math.round(x * Math.cos(angle) - y * Math.sin(angle));
                let y1 = Math.round(y * Math.cos(angle) + x * Math.sin(angle));
                if ((x1 < 0) || (y1 < 0) || (x1 >= cnv.width) || (y1 >= cnv.height))
                    continue;
                let id1 = ((image.width * y1) + x1) * 4;
                datac[id] = data[id1];
                datac[id+1] = data[id1+1];
                datac[id+2] = data[id1+2];
                datac[id+3] = data[id1+3];

            }
        }
        cnv.getContext('2d').putImageData(imageDataCNV, 0, 0);

    }
}

function transform_app() {

    let canvas = document.getElementById("canvas");
    console.log(this.width);
    canvas.width = this.width;
    canvas.height = this.height;
    let context = canvas.getContext('2d');
    // context.drawImage(this, 100, 0);

    let bg = document.createElement('canvas');
    bg.width = this.width;
    bg.height = this.height;
    let bg_ctx = bg.getContext('2d');
    bg_ctx.drawImage(this, 0, 0);

    //context.drawImage(bg, 0, 0);

    document.body.appendChild(document.createElement('br'));
    document.body.appendChild(document.createTextNode('Масштабирование'));
    document.body.appendChild(document.createElement('br'));

    let canvas_scale = document.createElement('canvas');
    document.body.appendChild(canvas_scale);
    canvas_scale.width = this.width;
    canvas_scale.height = this.height;
    let canvas_scale_ctx = canvas_scale.getContext('2d');
    scale = parseFloat(prompt('Введите масштаб от 0+ до 1.0: '));
    to_scale(bg, canvas_scale, scale);

    document.body.appendChild(document.createElement('br'));
    document.body.appendChild(document.createTextNode('Вращение'));
    document.body.appendChild(document.createElement('br'));

    let canvas_rotate = document.createElement('canvas');
    document.body.appendChild(canvas_rotate);
    canvas_rotate.width = this.width;
    canvas_rotate.height = this.height;
    let canvas_rotate_ctx = canvas_rotate.getContext('2d');
    let rotate = parseFloat(prompt('Введите угол от 0 до 360: '));

    // to_rotate(bg, canvas_rotate, parseFloat(prompt('Введите масштаб от 0+ до 1.0: ')));
    to_rotate(bg, canvas_rotate, rotate);



}


function previewFile() {
    var preview = document.querySelector('img');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
        preview.onload = transform_app.bind(preview);
    };

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}